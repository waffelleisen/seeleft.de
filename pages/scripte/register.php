<?php

if (isset($_GET['register'])) {
    $error = false;
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong><i class="fas fa-exclamation-triangle"></i></strong> Die E-Mail ist bereist in unserer Datenbank! Bitte verwende eine andere.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        $error = true;
    }

    if (strlen($password) == 0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong><i class="fas fa-exclamation-triangle"></i></strong> Du musst ein Passwort angeben!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        $error = true;
    }

    if ($password != $password2) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong><i class="fas fa-exclamation-triangle"></i></strong> Die Passwörter müssen übereinstimmen!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        $error = true;
    }

    //check email of doubels
    if (!$error) {
        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
        $result = $statement->execute(array('email' => $email));
        $user = $statement->fetch();

        if ($user != false) {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong><i class="fas fa-exclamation-triangle"></i></strong> Die E-Mail ist bereits vergeben! Bitte benutze eine andere.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            $error = true;
        }
    }

    //register user
    if (!$error) {
        $password_hash = password_hash($password, PASSWORD_DEFAULT);

        $statement = $pdo->prepare("INSERT INTO users (email, password, username) VALUES (:email, :password, :username)");
        $result = $statement->execute(array('email' => $email, 'password' => $password_hash, 'username' => $username));

        if ($result) {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong><i class="fas fa-check"></i></strong> Du wurdest erfolgreich registriert. <a href="#">Login</a><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        } else {
            echo '<div class="alert alert-info" role="alert">Beim abspeichern deiner Daten ist ein Fehler aufgetreten! °-°</div>';
        }
    }

}
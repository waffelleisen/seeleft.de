<?php

if (isset($_GET['login'])){
    $email = $_POST['email'];
    $password = $_POST['password'];

    $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $result = $statement->execute(array('email' => $email));
    $user = $statement->fetch();

    //check password
    if ($user !== false && password_verify($password, $user['password'])) {
        $_SESSION['userid'] = $user['id'];
        echo '<script language="JavaScript" type="text/javascript">document.location="https://spm-gruppe.de";</script>';
    } else {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong><i class="fas fa-exclamation-triangle"></i></strong> E-Mail oder Passwort ungültig.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
}

<?php include_once('../templates/loginModal.html'); ?>
<?php include_once('../templates/registerModal.html'); ?>

<!DOCTYPE html>
<html lang="de">
<head>

    <!-- cookie alert -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#ffffff",
                        "text": "#000000"
                    },
                    "button": {
                        "background": "#000000",
                        "text": "#ffffff"
                    }
                },
                "position": "bottom-left",
                "content": {
                    "message": "Diese Website nutzt Cookies, um unseren Service in voller Funktionalität bereitzustellen.",
                    "dismiss": "OK!",
                    "link": "Mehr darüber",
                    "href": "https://seeleft.de/datenschutz"
                }
            })});
    </script>
    <!-- cookie alert END -->

    <meta charset="UTF-8">
    <meta name="viewport" content= "width=device-width, initial-scale=1.0">


    <title>seeleft.de - Magazin</title>

    <!-- import font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Oswald|Roboto|Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../style/fonts.css">

    <!-- import css -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style/slft-footer.css">
    <link rel="stylesheet" href="../style/slft-style.css">
    <script src="https://kit.fontawesome.com/1764995292.js"></script>

</head>
<body>

<div class="container">

    <div class="row">
        <div id="navigator" class="col-sm-12">

            <ul class="nav justify-content-center slft-navbar">
                <li class="nav-item">
                    <a class="nav-link" href="../">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Zeitung</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kontakt</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mitmachen!</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Grafiker/-in</a>
                        <a class="dropdown-item" href="#">Autor/-in</a>
                        <a class="dropdown-item" href="#">Entwickler/-in</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#loginModal" data-toggle="modal" data-target="#loginModal">Login</a>
                        <a class="dropdown-item" href="#registerModal" data-toggle="modal" data-target="#registerModal">Registrieren</a>
                    </div>
                </li>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-white black-container">
            <h2>Impressum</h2>
                <b>Postanschrift:</b>
            <br>
                sachsen_photography Media Group
            <br>
                Postfach 11 03
            <br>
                09665 Frankenberg/ Sa.
            <br>
                <b>Angaben gemäß § 5 TMG:</b>
            <br>
                Erik Frank Hoffmann
            <br>
                sachsen_photography Medien Gruppe
            <br>
                Am Schloss 11
            <br>
                09669 Frankenberg/Sa.
            <br>
                <b>Telefon:</b> 017663840384
            <br>
                <b>E-Mail:</b> info@spm-gruppe.de
            <br>
                <b>Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:</b>
            <br>
                Tim Kempe (<a href="https://www.seeleft.de">www.seeleft.de</a>)
            <br>
                Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: Schlichtungsseite. Unsere E-Mail-Adresse finden Sie oben im Impressum.
            <br>
                Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
            <br>
                Haftung für Inhalte
            <br>
                Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Des weiteren weisen wir darauf hin, dass wir den Dienst der Seiten “wirsindmehr” durch Dritte verwalten lassen und schließen somit eine Haftung für diese Seiten unter dem Link wsm.spm-gruppe.de aus. Ferner werden diese Seiten aks Fremdinformationen angesehen, bei welchen wir nicht zur Überwachung verpflichtet sind.
            <br>
                Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
            <br>
                <b>Haftung für Links</b>
            <br>
                Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.
            <br>
                Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
            <br>
                <b>Urheberrecht</b>
            <br>
                Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nicht gestattet. Jegliche Nutzung von Informationen dieser Webseite und der Seiten im Angebot der SPM Gruppe sind Lizenzpflichtig und müssen mit dem Betreiber der Webseite abgesprochen werden. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
            <br>
            <br>
            Quellennutzung: <a href="https://www.e-recht24.de">https://www.e-recht24.de</a>
            </p>
        </div>
    </div>

    <div id="footer" class="row text-white">

        <div class="col-sm-4">
            <h3 style="text-transform: uppercase;">Seeleft</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et</p>
        </div>

        <div class="col-sm-4 slft-footer">
            <h3 style="text-transform: uppercase;">Projekt</h3>
            <a href="#">Über uns</a>
            <hr align="left">
            <a href="#">Partner</a>
        </div>

        <div class="col-sm-4 slft-footer">
            <h3 style="text-transform: uppercase">Rechtliches</h3>
            <a href="../impressum/">Impressum</a>
            <hr align="left">
            <a href="https://spm-gruppe.de/datenschutz/">Datenschutz</a>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12"><h6 class="font-titillium text-muted text-center">&copy; COPYRIGHT 2019. ALLE RECHTE VORBEHALTEN - SEELEFT (<i class="fas fa-cog fa-spin"></i> Version: 0.0.5)</h6></div>
    </div>

</div>

<!-- livechat modul -->
<!--<script src="//code.tidio.co/uikquzefehvjeompezcfmanhoks0ulsj.js"></script>-->
<!-- livechat modul END -->

<!-- import js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- import js END -->

</body>
</html>
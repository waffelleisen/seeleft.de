<?php
//db default config
require_once('config/db.php');

//config file
require_once('config/config.php');

//load templates
include_once('templates/loginModal.html');
include_once('templates/registerModal.html');

//load php scripts
include_once('pages/scripte/register.php');
include_once('pages/scripte/login.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>

    <!-- cookie alert -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#ffffff",
                        "text": "#000000"
                    },
                    "button": {
                        "background": "#000000",
                        "text": "#ffffff"
                    }
                },
                "position": "bottom-left",
                "content": {
                    "message": "Diese Website nutzt Cookies, um unseren Service in voller Funktionalität bereitzustellen.",
                    "dismiss": "OK!",
                    "link": "Mehr darüber",
                    "href": "https://seeleft.de/datenschutz"
                }
            })});
    </script>
    <!-- cookie alert END -->

    <meta charset="UTF-8">
    <meta name="viewport" content= "width=device-width, initial-scale=1.0">


    <title>seeleft.de - Magazin</title>

    <!-- import font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Oswald|Roboto|Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="style/fonts.css">

    <!-- import css -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="style/slft-footer.css">
    <link rel="stylesheet" href="style/slft-style.css">
    <script src="https://kit.fontawesome.com/1764995292.js"></script>

</head>
<body>

<div class="container">

    <div class="row">
        <div id="navigator" class="col-sm-12">

            <ul class="nav justify-content-center slft-navbar">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Zeitung</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kontakt</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mitmachen!</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Grafiker/-in</a>
                        <a class="dropdown-item" href="#">Autor/-in</a>
                        <a class="dropdown-item" href="#">Entwickler/-in</a>
                    </div>
                </li>
                <?php if($showUser) { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#loginModal" data-toggle="modal" data-target="#loginModal">Login</a>
                        <a class="dropdown-item" href="#registerModal" data-toggle="modal" data-target="#registerModal">Registrieren</a>
                    </div>
                </li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-white black-container">
            <h2>Neue Headline</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero</p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-white black-container">
            <h2>Neue Headline</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero</p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-white black-container">
            <h2>Neue Headline</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero</p>
        </div>
    </div>

    <div id="footer" class="row text-white">

        <div class="col-sm-4">
            <h3 style="text-transform: uppercase;">Seeleft</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et</p>
        </div>

        <div class="col-sm-4 slft-footer">
            <h3 style="text-transform: uppercase;">Projekt</h3>
            <a href="#">Über uns</a>
            <hr align="left">
            <a href="#">Partner</a>
        </div>

        <div class="col-sm-4 slft-footer">
            <h3 style="text-transform: uppercase">Rechtliches</h3>
            <a href="impressum/">Impressum</a>
            <hr align="left">
            <a href="https://spm-gruppe.de/datenschutz/">Datenschutz</a>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12"><h6 class="font-titillium text-muted text-center">&copy; COPYRIGHT 2019. ALLE RECHTE VORBEHALTEN - SEELEFT (<i class="fas fa-cog fa-spin"></i> Version: 0.0.5)</h6></div>
    </div>

</div>

<!-- livechat modul -->
<!--<script src="//code.tidio.co/uikquzefehvjeompezcfmanhoks0ulsj.js"></script>-->
<!-- livechat modul END -->

<!-- import js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- import js END -->

</body>
</html>